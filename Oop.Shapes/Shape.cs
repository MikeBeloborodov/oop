﻿using System;

namespace Oop.Shapes
{
	public abstract class Shape
	{
		/// <summary>
		/// Площадь фигуры
		/// </summary>
		public virtual double Area { get; }

		/// <summary>
		/// Периметр
		/// </summary>
		public virtual double Perimeter { get; }

		/// <summary>
		/// Количество вершин
		/// </summary>
		public virtual int VertexCount { get; }

		public virtual bool IsEqual(Shape shape) => false;
	}

	public class Circle : Shape
	{
		public Circle(int radius)
		{
			Radius = radius;
			Area = Math.PI * Math.Pow(radius, 2);
			Perimeter = 2 * Math.PI * radius;
		}
		public int Radius { get; }
		public override double Area { get; }
		public override double Perimeter { get; }

		public override bool IsEqual(Shape shape)
		{
			if (shape.Area == Area)
			{
				return true;
			}
			return false;
		}
	}

	public class Triangle : Shape
	{
		public Triangle(int a, int b, int c)
        {
			SideA = a;
			SideB = b;
			SideC = c;
			Perimeter = a + b + c;
			SemiPerimeter = Perimeter / 2;
			Area = Math.Sqrt(SemiPerimeter * (SemiPerimeter - a) * (SemiPerimeter - b) * (SemiPerimeter - c));
			VertexCount = 3;
        }
		public int SideA { get; set; }
		public int SideB { get; set; }
		public int SideC { get; set; }
		public double SemiPerimeter { get; set; }
		public override double Area { get; }
		public override double Perimeter { get; }
		public override int VertexCount { get; }


		public override bool IsEqual(Shape shape)
		{
			if (shape.Area == Area)
			{
				return true;
			}
			return false;
		}
	}

	public class Square : Shape
	{
		public Square(int a)
        {
			Side = a;
			Area = Math.Pow(a, 2);
			Perimeter = a * 4;
			VertexCount = 4;
        }
		public int Side { get; set; }
		public override double Area { get; }
		public override double Perimeter { get; }
		public override int VertexCount { get; }

		public override bool IsEqual(Shape shape)
		{
			if (shape.Area == Area)
			{
				return true;
			}
			return false;
		}
	}

	public class Rectangle : Shape
	{
		public Rectangle(int a, int b)
        {
			SideA = a;
			SideB = b;
			VertexCount = 4;
			Area = a * b;
			Perimeter = 2 * (a + b);
        }

		public int SideA { get; set; }
		public int SideB { get; set; }
		public override double Area { get; }
		public override double Perimeter { get; }
		public override int VertexCount { get; }
		public override bool IsEqual(Shape shape)
		{
			if (shape.Area == Area)
			{
				return true;
			}
			return false;
		}
	}
}
