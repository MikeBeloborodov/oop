﻿using System;

namespace Oop.Shapes.Factories
{
	public class ShapeFactory
	{
		public Shape CreateCircle(int radius)
		{
			if (radius <= 0)
            {
				throw new ArgumentOutOfRangeException("This radius is invalid");
            }
			Shape circle = new Circle(radius);
			return circle;
		}

		public Shape CreateTriangle(int a, int b, int c)
		{
			if(a <= 0 || b <= 0 || c <=0)
            {
				throw new ArgumentOutOfRangeException("These sides are invalid");
			}

			if( (a + b) < c || (a + c) < b || (c + b) < a)
            {
				throw new InvalidOperationException("These sides are invalid");
			}

			Shape triangle = new Triangle(a, b, c);
			return triangle;
		}

		public Shape CreateSquare(int a)
		{
			if (a <= 0)
			{
				throw new ArgumentOutOfRangeException("This side is invalid");
			}
			Shape square = new Square(a);
			return square;
		}

		public Shape CreateRectangle(int a, int b)
		{
			if (a <= 0 || b <= 0)
			{
				throw new ArgumentOutOfRangeException("These sides are invalid");
			}
			Shape rectangle = new Rectangle(a, b);
			return rectangle;
		}
	}
}